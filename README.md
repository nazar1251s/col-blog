## Symfony web-application project ##



### Вимоги ###
* версія php > 7.3

### Установка ###
* `git clone https://nazar1251s@bitbucket.org/nazar1251s/web-application.git projectname`
* `cd projectname`
* `composer install`

* Створіть базу даних та підключіть її в файлі .env
* зробіть міграцію таблиць в базу даних:
* `php bin/console make:migration`
* `php bin/console doctrine:migrations:migrate`

* примітка: щоб не створювати базу даних вручну можна ввести назву бази в файлі .env і скористатися командою:
* `php bin/console doctrine:database:create`


* `php bin/console server:run`для запуску програми на http://localhost:8000/ 


### Роботи з webpack Encore ###
### вимоги: ###
* встановлений глобально [Node.js](https://nodejs.org/en/download/), та менеджер пакетів [Yarn](https://yarnpkg.com/getting-started/install)

### встановлення: ###
* `yarn install` для установки компонентів

* `yarn encore dev` для одноразового запуску
* `yarn encore dev --watch` для постійного моніторингу



** Карта сайта: **
* 1. Адмін панель { /admin } [логін: admin, пароль: 12], я записав дані входу в провайдер пам'яті
* 2. На головній сторінці відображено всі записи та їхні автори, після реєстрації та входу надається можливість створювати 
редагувати та видаляти свої записи, присутнє налаштування профілю





