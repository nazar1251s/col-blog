<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserType;
use App\Entity\User;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class CrudUserController extends AbstractController
{


    /**
     * Виводить всіх користувачів
     *
     * @Route("/admin/all-users", name="all_users")
     */
    public function readUser(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('admin/user/all_users.html.twig', [
            'users' => $users,
        ]);
    }



    /**
     * Створює користувача
     *
     * @Route("/admin/create-user", name="create_user")
     */
    public function createUser(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $new_user = new User();

        // форма
        $form = $this->createForm(UserType::class, $new_user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($new_user, $new_user->getPlainPassword());
            $new_user->setPassword($password);

            // зберігаємо
            $entityUser = $this->getDoctrine()->getManager();
            $entityUser->persist($new_user);
            $entityUser->flush();

            return $this->redirectToRoute('all_users');
        }

        return $this->render('admin/user/user_form.html.twig', [
            'form' => $form->createView()
        ]);
    }



    /**
     * Оновлює дані користувача
     *
     * @Route("/admin/update-user/{id}", name="update_user")
     */
    public function updateUser(Request $request, $id ): Response
    {
        $entityUser = $this->getDoctrine()->getManager();
        $user = $entityUser->getRepository(User::class)->find($id);

        // форма
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // зберігаємо
            $entityUser->persist($user);
            $entityUser->flush();

            return $this->redirectToRoute('all_users');
        }

        return $this->render('admin/user/user_form.html.twig', [ 'form' => $form->createView() ] );


    }


    /**
     * Видаляє користувача
     *
     * @Route("/admin/delete-user/{id}", name="delete_user")
     */
    public function deleteUser(User $user): Response
    {

        $entityUser = $this->getDoctrine()->getManager();
        $entityUser->remove($user);
        $entityUser->flush();

        return $this->redirectToRoute('all_users');

    }


}
