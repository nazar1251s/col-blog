<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;



class SecurityController extends AbstractController
{

    /**
     * Виводить всіх користувачів
     *
     * @Route("/admin", name="admin_panel")
     */
    public function adminPanel(): Response
    {
        return $this->render('admin/admin_panel.html.twig');
    }

    /**
     * Авторизує адміністратора
     *
     * @Route("/admin/login", name="admin_authorization")
     */
    public function adminAuthorization(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('admin/admin_authorization.html.twig', ['error' => $error]);
    }

}
