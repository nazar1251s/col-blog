<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use App\Form\PostType;
use App\Form\UserType;
use App\Form\UserImgType;
use App\Entity\Post;
use App\Entity\User;
use App\Entity\UserImg;



class CrudPostController extends AbstractController
{


    /**
     * Створює запис
     *
     * @Route("/profile/create-post", name="create_post")
     */
    public function createPost(Request $request, Security $security): Response
    {
        $user_id = $security->getUser()->getId();
        $new_post = new Post();

        // форма
        $form = $this->createForm(PostType::class, $new_post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // записуємо дату створення запису
            $new_post->setCreatedAt(new \DateTime());

            // записуємо id користувача який створив запис
            $new_post->setUserId( $user_id );

            // зберігаємо
            $entityPost = $this->getDoctrine()->getManager();
            $entityPost->persist($new_post);
            $entityPost->flush();

            return $this->redirectToRoute('profile');
        }

        return $this->render('user/post_form.html.twig', [
            'form' => $form->createView()
        ]);
    }



    /**
     * Оновлює запис
     *
     * @Route("/profile/update-post/{id}", name="update_post")
     */
    public function updatePost(Request $request, $id ): Response
    {
        $entityPost = $this->getDoctrine()->getManager();
        $post = $entityPost->getRepository(Post::class)->find($id);

        // форма
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // записуємо дату оновлення запису
            $post->setUpdatedAt(new \DateTime());

            // зберігаємо
            $entityPost->persist($post);
            $entityPost->flush();

            return $this->redirectToRoute('profile');
        }

        return $this->render('user/post_form.html.twig', [ 'form' => $form->createView() ] );


    }


    /**
     * Видаляє запис
     *
     * @Route("/profile/delete-post/{id}", name="delete_post")
     */
    public function deletePost(Post $post): Response
    {

        $entityPost = $this->getDoctrine()->getManager();
        $entityPost->remove($post);
        $entityPost->flush();

        return $this->redirectToRoute('profile');

    }



}