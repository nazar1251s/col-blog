<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Post;
use App\Entity\User;

class MainPageController extends AbstractController
{

    /**
     * Виводить на головну сторінку всі записи які колись були створені
     * без можливості редагувати
     * Містить ссилки для переходу на форму для авторизації чи реєєстрації
     * для подальшої можливості створювати видаляти та редагувати записи
     *
     * @Route("/", name="main_page")
     */
    public function inde(): Response
    {

        $posts = $this->getDoctrine()->getRepository(Post::class)->findAll();
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();


        return $this->render('base.html.twig', [
            'posts' => $posts,
            'users' => $users,
        ]);

    }

    /**
     *
     * @Route("/logout", name="logout")
     */
    public function logout(): void
    {
        throw new \LogicException();
    }

}
