<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use App\Form\PostType;
use App\Form\UserType;
use App\Form\UserImgType;
use App\Entity\Post;
use App\Entity\User;
use App\Entity\UserImg;



class ProfileController extends AbstractController
{


    /**
     * Виводить профіль та записи створені поточним користувачем
     *
     * @Route("/profile", name="profile")
     */
    public function profile(Security $security): Response
    {

        $posts = $this->getDoctrine()->getRepository(Post::class)->findAll();
        $user_id = $security->getUser()->getId();
        $user_name = $security->getUser();
        $user_email = $security->getUser()->getEmail();

        $profile_img =  $this->getDoctrine()->getRepository(User::class)->findOneBy(array('id' => $user_id));
        $image_name = $profile_img->getImageName();

        return $this->render('user/user_profile.html.twig', [
            'posts' => $posts,
            'user_id' => $user_id,
            'user_name' => $user_name,
            'user_email' => $user_email,
            'profile_img' => $profile_img,
            'image_name' => $image_name,

        ]);


    }


    /**
     * Оновлює профіль
     *
     * @Route("/profile/update-user/{id}", name="updates_profile")
     */
    public function updateUser(Request $request, $id ): Response
    {
        $entityUser = $this->getDoctrine()->getManager();
        $user = $entityUser->getRepository(User::class)->find($id);

        // форма
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // зберігаємо
            $entityUser->persist($user);
            $entityUser->flush();

            return $this->redirectToRoute('profile');
        }

        return $this->render('user/user_updates_form.html.twig', [
            'form' => $form->createView()
        ] );


    }

    /**
     * Керує зображеннями юзера
     *
     * @Route("/profile/profile-img", name="profile_img")
     */
    public function profileImg(Request $request, Security $security): Response
    {

        $user_id = $security->getUser()->getId();
        $imgs = $this->getDoctrine()->getRepository(UserImg::class)->findAll();

        $new_img = new UserImg();
        // форма
        $form = $this->createForm(UserImgType::class, $new_img);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // записуємо id користувача який загрузив фото
            $new_img->setUserId( $user_id );

            // записуємо дату загрузки фотографії
            $new_img->setCreatedAt(new \DateTime());

            // зберігаємо
            $entityUserImg = $this->getDoctrine()->getManager();
            $entityUserImg->persist($new_img);
            $entityUserImg->flush();

            return $this->redirectToRoute('profile_img');
        }

        return $this->render('user/profile_img.html.twig', [
            'form' => $form->createView(),
            'user_id' => $user_id,
            'imgs' => $imgs,

        ]);

    }


    /**
     * вибирає зображення для заставки
     *
     * @Route("/profile/profile-img/choose/{user_id}/{imagename}", name="profile_img_choose")
     */
    public function profileImgChoose($user_id, $imagename): Response
    {
        $entityUserImg = $this->getDoctrine()->getManager();
        $post = $entityUserImg->getRepository(User::class)->find($user_id);

        // записуємо imagename в таблицю користувача
        $post->setImageName( $imagename );

        // зберігаємо
        $entityUserImg->persist($post);
        $entityUserImg->flush();


        return $this->redirectToRoute('profile_img');
    }



    /**
     * видаляє зображення юзера
     *
     * @Route("/profile/profile-img/delete/{id}", name="profile_img_delete")
     */
    public function profileImgDelete(UserImg $userimg, Security $security): Response
    {
        $entityUserImg = $this->getDoctrine()->getManager();

        // назва картинки що на аві юзера
        $ImageName = $security->getUser()->getImageName();
        // назва того що видаляємо
        $Use = $userimg->getImageName();

        if ($ImageName == $Use) {

            $user_id = $security->getUser()->getId();
            $user = $entityUserImg->getRepository(User::class)->find($user_id);
            // 'онуляємо' imagename в таблиці користувача
            $user->setImageName(NULL);
            // зберігаємо
            $entityUserImg->persist($user);
        }

        $entityUserImg->remove($userimg);
        $entityUserImg->flush();

        return $this->redirectToRoute('profile_img');
    }




}