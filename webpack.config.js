var Encore = require('@symfony/webpack-encore');

Encore

    // де збережені компільовані файли
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSingleRuntimeChunk()
    // очистити outputPath dir перед кожною побудовою
    .cleanupOutputBeforeBuild()


    // створює css і js
    .addEntry('base', './assets/js/base.js')
    .addEntry('admin', './assets/js/admin.js')

    // тільки css (але не рекомендується)
    //.addStyleEntry('base', './assets/js/base.js')

;

module.exports = Encore.getWebpackConfig();